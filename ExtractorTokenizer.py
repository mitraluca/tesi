import pandas as pd
import psycopg2
import spacy
from docutils.nodes import Body
from pymongo import MongoClient
import mysql.connector
from spacy_langdetect import LanguageDetector
import gensim
import math


class ExtractorTokenizer:
    def extract_mysql(self, connection, query, tokenMethod, key,name_attribute = True):
        try:
            results_query = pd.read_sql_query(query, connection, index_col=key)
            tokenMethod = tokenMethod.lower()
            tokenMethod = self._extract_lemma(tokenMethod)
            #token.text, token.lemma_, token.pos_,token.tag_, token.dep_,token.shape_, token.is_alpha, token.is_stop
            results = dict()

            recognizeLanguage = spacy.load("en")
            recognizeLanguage.add_pipe(LanguageDetector(), name="language detector")
            for indicatorName, row in results_query.iterrows():
                if results.get(indicatorName) is None:
                    results[indicatorName] = list()
                    self._insert_in_list_atomic_value(key,results[indicatorName], name_attribute, recognizeLanguage, tokenMethod,indicatorName)
                for column in results_query.columns:
                    self._insert_in_list_atomic_value(column, results[indicatorName], name_attribute, recognizeLanguage, tokenMethod, row[column])
        except Exception as e:
            print("ERROR {}".format(e))
        return results

    def _insert_in_list_atomic_value(self, column, list_generated, name_attribute, recognizeLanguage, tokenMethod, valore):
        if type(valore) is not str:
            string_to_add = self._generate_string(name_attribute, column, str(valore))
            if not string_to_add in list_generated:
                list_generated.append(string_to_add)
        else:
            data = valore.split()
            if len(data) <= 1:
                string_to_add = self._generate_string(name_attribute, column, valore)
                if not string_to_add in list_generated:
                    list_generated.append(string_to_add)
            else:
                string_to_add = self._generate_complete_string(recognizeLanguage, column, data, valore, tokenMethod,
                                                        name_attribute)
                if not string_to_add in list_generated:
                    list_generated.append(string_to_add)

    def _extract_lemma(self, tokenMethod):
        if tokenMethod == "text" or tokenMethod == "is_alpha" or tokenMethod == "is_stop":
            tokenMethod = "token." + tokenMethod
        elif tokenMethod == "lemma" or tokenMethod == "pos" or tokenMethod == "tag" or tokenMethod == "dep" or tokenMethod == "shape":
            tokenMethod = "token." + tokenMethod + "_"
        return tokenMethod

    def extract_mongo_db(self,cursor, tokenMethod, name_attribute=True):
        tokenMethod = self._extract_lemma(tokenMethod)
        recognizeLanguage = spacy.load("en")
        recognizeLanguage.add_pipe(LanguageDetector(), name="language detector")
        result = dict()
        i = 1
        for data in cursor:
            i = i+1
            key = str(data["_id"])
            list_generated = list()
            for k,v in data.items():
                if type(v) is dict:
                    self._add_attributes_string_from_dict(k, v, tokenMethod, name_attribute, recognizeLanguage, list_generated)
                elif type(v) is str:
                    dataString = v.split()
                    if len(dataString)<=1:
                        list_generated.append(self._generate_string(name_attribute,k,v))
                    else:
                        list_generated.append(self._generate_complete_string(recognizeLanguage,k,dataString,v,tokenMethod,name_attribute))
                elif type(v) is list:
                    if name_attribute:
                        stringToInsert = k+"_"
                    else:
                        stringToInsert=""
                    for valore in v:
                        if type(valore) is dict:
                            self._add_attributes_string_from_dict(k, valore, tokenMethod, name_attribute, recognizeLanguage,list_generated)
                        elif type(valore) is str:
                            dataString = valore.split()
                            if len(dataString) <= 1:
                                list_generated.append(self._generate_string(name_attribute, k, valore))
                            else:
                                list_generated.append(self._generate_complete_string(recognizeLanguage,k,dataString,valore,tokenMethod,name_attribute))
                        else:
                            list_generated.append(stringToInsert+str(valore))
                else:
                    list_generated.append(self._generate_string(name_attribute,k,str(v)))
            result[key] = list_generated
        return result

    def _add_attributes_string_from_dict(self, k, v, tokenMethod, name_attribute, recognizeLanguage, listToAdd):
        if name_attribute:
            stringToInsert = k+"_"
        else:
            stringToInsert=""
        for key, value in v.items():
            if type(value) is dict:
                self._add_attributes_string_from_dict(stringToInsert + key, value, tokenMethod, name_attribute, recognizeLanguage,listToAdd)
            elif type(value) is list:

                  stringaDaGenerare=""
                  for valore in value:
                      if type(valore) is dict:
                          stringaDaGenerare = stringaDaGenerare+" "+stringToInsert+ self._add_attributes_string_from_dict(key, valore, tokenMethod, name_attribute, recognizeLanguage)
                      elif type(valore) is str:
                          data = valore.split()
                          if len(data) <= 1:
                              stringaDaGenerare = stringaDaGenerare+" "+stringToInsert+self._generate_string(name_attribute, key, valore)
                          else:
                              stringaDaGenerare = stringaDaGenerare+" "+stringToInsert+ self._generate_complete_string(recognizeLanguage,key,data, valore,tokenMethod,name_attribute)
                      else:
                          stringaDaGenerare = stringaDaGenerare+" "+stringToInsert+str(valore)
                  listToAdd.append(stringaDaGenerare)

            elif type(value) is str:
                data = value.split()
                if len(data) <= 1:
                    listToAdd.append(self._generate_string(name_attribute,stringToInsert+key ,value))
                else:
                    listToAdd.append(self._generate_complete_string(recognizeLanguage,stringToInsert+key,data,value,tokenMethod,name_attribute))
            else:
                listToAdd.append(self._generate_string(name_attribute,stringToInsert+key,str(value)))



    def _generate_complete_string(self, recognizeLanguage, column, data, valore, tokenMethod, name_attribute):
        finally_string = str()
        language = recognizeLanguage(valore)
        language_recognized = language._.language["language"]
        if language_recognized not in ("en","it"):
            language_recognized = "it"
        nlp = spacy.load(language_recognized)
        for dato in data:
            doc = nlp(dato)
            try:
                for token in doc:
                    finally_string = finally_string + self._generate_string(name_attribute, column, eval(tokenMethod) + " ")
            except Exception as e:
                finally_string=finally_string+self._generate_string(name_attribute,column,dato)
        return finally_string

    def _generate_string(self, name_attribute, column, string):
        if name_attribute:
            return column +"_" + string
        return string

    def to_print(self, dictionary_to_print):
        global key, val, valore
        result = str()
        for key, val in dictionary_to_print.items():
            result = result + "{}->\n".format(key)
            for valore in val:
                result = result + "\t {}\n".format(valore)
        return result

    def to_string(self, dictionary_to_analize):
        global key, val, valore
        result = str()
        for key, val in dictionary_to_analize.items():
            result = result + "{}-> ".format(key)
            for valore in val:
                result = result + "{} ".format(valore)
        return result

    def construct_array_of_string_from_dictionary(self, dictionary):
        result = []
        for key, val in dictionary.items():
            result.append(key+"->")
            for valore in val:
                result.append(valore)
        return result

    def _construct_string_of_words_from_dictionary_and_key(self, dictionary, key):
        result = str()
        valore = dictionary[key]
        for val in valore:
            result = result+ " "+val
        return result

    def get_bag_of_words_of_key(self,dictionary,key):
        string_of_key = self._construct_string_of_words_from_dictionary_and_key(dictionary,key)
        gens = gensim.corpora.Dictionary([string_of_key.split()])
        bag_of_words = gens.doc2bow(string_of_key.split())
        words = gens.token2id
        bag_of_words = dict(bag_of_words)
        result = dict()
        for k, v in words.items():
            if v in bag_of_words.keys():
                result[k] = bag_of_words[v]
        return result

    def compare_sql_width_mongo(self,dictionarySql,dictionaryMongo):
        string_mongo = self.to_string(dictionaryMongo)
        string_sql = self.to_string(dictionarySql)
        gens = gensim.corpora.Dictionary([string_mongo.split()])
        gens.add_documents([string_sql.split()])
        countSql = gens.doc2bow(string_sql.split())
        countMongo = gens.doc2bow(string_mongo.split())
        differencesFromMongo = list(set(countMongo) - set(countSql))
        differencesFromMongo = dict(differencesFromMongo)
        differencesFromSql = list(set(countSql) - set(countMongo))
        differencesFromSql = dict(differencesFromSql)
        words = gens.token2id
        result=dict()
        for k, v in words.items():
            if v in differencesFromMongo.keys() and v in differencesFromSql.keys():
                partialResult = dict()
                partialResult["mongo"] = differencesFromMongo[v]
                partialResult["sql"] = differencesFromSql[v]
                result[k] = partialResult
            elif v in differencesFromMongo.keys():
                partialResult = dict()
                partialResult["mongo"] = differencesFromMongo[v]
                partialResult["sql"] = 0
                result[k] = partialResult
            elif v in differencesFromSql.keys():
                partialResult = dict()
                partialResult["mongo"] = 0
                partialResult["sql"] = differencesFromSql[v]
                result[k] = partialResult
        return result

    def calculate_cosine_similarity_bagofwords(self, BoW1, BoW2):
        listNoIn1 = list(BoW2.keys() - BoW1.keys())
        for key in listNoIn1:
            BoW1[key] = 0
        listNoIn2 = list(BoW1.keys() - BoW2.keys())
        for key in listNoIn2:
            BoW2[key] = 0
        BoW3 = dict()
        for key in BoW1.keys():
            BoW3 [key] = BoW1[key] * BoW2[key]
        for key in BoW1.keys():
            BoW1[key] = BoW1[key] * BoW1[key]
            BoW2[key] = BoW2[key] * BoW2[key]
        return sum(BoW3.values()) / (math.sqrt(sum(BoW1.values())) * math.sqrt(sum(BoW2.values())))



if __name__ == "__main__":
    result = ExtractorTokenizer()
    # mongodb+srv://mira:<password>@tesi-v0zi6.mongodb.net/test?retryWrites=true&w=majority
    client = MongoClient('mongodb+srv://mira:mira1996@tesi-v0zi6.mongodb.net/test?retryWrites=true&w=majority')
    db = client.boardgames
    boardgames =db.boardgames.find(
        {"$or": [{"_id":3400}, {"_id": 129800}]}
        ,{"title":1, "_id":1}
    )#.limit(4)
    dictionaryMongo = result.extract_mongo_db(boardgames, "text", True)
    print(dictionaryMongo)
    print("MONGO_DB:")
    print(result.get_bag_of_words_of_key(dictionaryMongo, "3400"))
    #print(result.get_bag_of_words_of_key(dictionaryMongo, "52000"))
    print(result.get_bag_of_words_of_key(dictionaryMongo, "129800"))
    #print(result.get_bag_of_words_of_key(dictionaryMongo, "68400"))

    credentials = {
        'host': 'island.ricerca.di.unimi.it',
        'database': 'maverick_iris_unimi_pubs',
        'user': 'islab',
        'password': 'civIIIdb'}
    connection = psycopg2.connect(**credentials)

    connection2 = mysql.connector.connect(host='test.logicastudio.net',
                                          database='LucaMiragliotta',
                                          user='lucamiragliotta',
                                          password='lucam123!')

    dictionarySql = result.extract_mysql(connection2, """
            SELECT b.b_id as '_id',b.b_title as title,b.b_description as description,b.b_max_players as max_players,b.b_min_players as min_players,b.b_p_time as p_time,b.b_year as 'year',p.p_id as 'publisher_code',p.p_name as publisher_name,c.c_name as categories
            from Boardgames b INNER JOIN Publisher p on b.b_p_id=p.p_id
            INNER JOIN Boardgames_categories bc on b.b_id = bc.bc_b_id
            INNER JOIN Categories c on c.c_id = bc.bc_c_id
            WHERE b.b_id IN (3400, 129800)
    """, "text",  "_id")
    dictionarySql = result.extract_mysql(connection2, """
    SELECT b_id as '_id', b_title as title
    FROM Boardgames
    WHERE b_id IN (3400, 60500)
    """, "lemma", "_id")
    print("Bag of words MYSQL: ")
    #print(result.get_bag_of_words_of_key(dictionarySql, 1))
    print(result.get_bag_of_words_of_key(dictionarySql, 3400))
    print(result.get_bag_of_words_of_key(dictionarySql, 60500))
    #print(result.get_bag_of_words_of_key(dictionarySql, 52000))
    #print(result.get_bag_of_words_of_key(dictionarySql, 129800))
    #print(result.get_bag_of_words_of_key(dictionarySql, 68400))
    """print(result.get_bag_of_words_of_key(dictionarySql, 126200))
    print(result.get_bag_of_words_of_key(dictionarySql, 129800))
    print(result.get_bag_of_words_of_key(dictionarySql, 133900))
    print(result.get_bag_of_words_of_key(dictionarySql, 155500))
    print(result.get_bag_of_words_of_key(dictionarySql, 10))
    print(result.get_bag_of_words_of_key(dictionarySql, 11))
    print(result.get_bag_of_words_of_key(dictionarySql, 12))
    print(result.get_bag_of_words_of_key(dictionarySql, 13))
    print(result.get_bag_of_words_of_key(dictionarySql, 14))
    print(result.get_bag_of_words_of_key(dictionarySql, 15))"""
    print(result.calculate_cosine_similarity_bagofwords(result.get_bag_of_words_of_key(dictionarySql, 3400), result.get_bag_of_words_of_key(dictionaryMongo, "3400")))
    #print(result.to_print(dictionarySql))

    #print(result.compare_sql_width_mongo(dictionaryMongo=dictionaryMongo, dictionarySql= dictionarySql))
    #print(result.compare_sql_width_mongo(dictionaryMongo=result.get_bag_of_words_of_key(dictionaryMongo, "60500"), dictionarySql= result.get_bag_of_words_of_key(dictionarySql, 60500)))




