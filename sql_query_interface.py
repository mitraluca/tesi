from typing import List

import psycopg2


class MaverickIrisSQLQueryInterface(object):
    _SELECT = "SELECT "
    _TABLES = "iris_publication "

    def __init__(self, credentials_dictionary: dict, field_list: List[str]):
        """
        This class is an interface to automate joins on the Maverick Iris database

        :param credentials_dictionary: a dictionary with the credentials to be passed to PostgreSQL
        :param field_list: contains the requested fields from the "iris_publication" table in the database as str
        """
        self._credential_dict = credentials_dictionary
        self._field_list = field_list

        self._connection = None
        self._cursor = None

        self._query = self._construct_textual_query()

    def _construct_textual_query(self):
        for campo in self._field_list:
            self._SELECT += campo + ","
            table_name = campo.split(".")[0]
            if ((table_name == "iris_type" or table_name == "pub_type" or table_name == "dissemination_type" or
                 table_name == "venue" or table_name == "publisher" or table_name == "sub_type" or table_name == "ssd") and self._TABLES.find(
                table_name) == -1):
                self._TABLES += " LEFT JOIN " + table_name + " on iris_publication." + table_name + "_id = " + table_name + ".id"
            if ((
                    table_name == "cun_area" or table_name == "competition_area" or table_name == "affiliation") and self._TABLES.find(
                    "author") == -1 and self._TABLES.find(
                    "cun_area") == -1 and self._TABLES.find("competition_area") == -1):
                self._TABLES += " LEFT JOIN contribution on iris_publication.id = contribution.pub_id LEFT JOIN author on contribution.author_id = author.id LEFT JOIN " + \
                                table_name + " ON author." + table_name + "_id = " + table_name + ".id"
            if ((
                    table_name == "cun_area" or table_name == "competition_area" or table_name == "affiliation") and self._TABLES.find(
                    "author") != -1 and self._TABLES.find(table_name) == -1):
                self._TABLES += " LEFT JOIN " + table_name + " ON author." + table_name + "_id = " + table_name + ".id"
            if (table_name == "author" and self._TABLES.find(table_name) == -1):
                self._TABLES += " LEFT JOIN contribution on iris_publication.id = contribution.pub_id LEFT JOIN author on contribution.author_id = author.id"
            if ((table_name == "keyword" or table_name == "abstract") and self._TABLES.find(table_name) == -1):
                self._TABLES += " LEFT JOIN " + table_name + " ON iris_publication.id=" + table_name + ".pub_id"
        self._SELECT = self._SELECT[:-1]

        print(self._SELECT)
        print("FROM " + self._TABLES)

        query = self._SELECT + " FROM " + self._TABLES
        return query

    def _connect_to_db_and_exec_query(self, credentials_dict):
        self._connection = psycopg2.connect(**credentials_dict)
        self._cursor = self._connection.cursor()
        self._cursor.execute(self._query)

    def __iter__(self):
        return self

    def __next__(self) -> dict:
        """
        This dictionary contains as keys the fields requested by the user
        and their values accordingly retrieved
        :return:
        """
        if self._cursor is None or self._connection is None:
            self._connect_to_db_and_exec_query(self._credential_dict)

        # fetchone() returns None if no elements remain
        next_element = self._cursor.fetchone()
        if next_element is not None:
            return next_element
        else:
            if self._cursor is None:
                self._cursor.close()
                self._cursor = None
            if self._connection is not None:
                self._connection.close()
                self._connection = None
            raise StopIteration


if __name__ == "__main__":

    _CREDENTIALS = {
        'host': 'island.ricerca.di.unimi.it',
        'database': 'maverick_iris_unimi_pubs',
        'user': 'islab',
        'password': 'civIIIdb'}
    my_list = ['iris_publication.id', 'sub_type.description', 'cun_area.description', 'competition_area.description',
               'author.a_surname', 'keyword.keyword', 'venue.title', 'affiliation.department', 'sub_type.description']
    for _ in MaverickIrisSQLQueryInterface(_CREDENTIALS, my_list):
        pass

    test = MaverickIrisSQLQueryInterface(_CREDENTIALS, my_list)
    print("I'm connected to db")

    for entry in zip(test, range(15)):
        print(entry)
